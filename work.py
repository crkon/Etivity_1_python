# list of the software positions in dublin and non-dublin area. Each position
# with annual salary and daily rate
dublin_area = [["Software Engineer", 100000, 575], ["Technical Architect", 87500, 575], ["Software Team Lead", 80000, 513], ["UI Developer", 65000, 415], ["Mid Level .NET", 56500, 400], ["C++ Developer", 63000, 435], ["Mid Level Python", 56500, 440], ["Android Developer",56500, 440],["Game Designer", 50000, 400], ["QA Lead", 65000, 400], ["QA Engineer", 47000, 325], ["QA Automation Engineer", 55000, 400]]
regional_area = [["Software Engineer", 90000, 525], ["Technical Architect", 85000, 535], ["Software Team Lead", 72000, 500], ["UI Developer", 58000, 400], ["Mid Level .NET", 51500, 375], ["C++ Developer", 58000, 415], ["Mid Level Python", 52500, 410], ["Android Developer",53500, 400],["Game Designer", 48000, 350], ["QA Lead", 58000, 380], ["QA Engineer", 43000, 300], ["QA Automation Engineer", 51000, 390]]

#collection
engineer_annual_dublin = []
engineer_annual_regional = []
engineer_daily_dublin = []
engineer_daily_regional = []
engineer_output_dublin = []
engineer_output_regional = []


# search for world software
engineer = 'Engineer'
for word in dublin_area:
    if engineer in word[0]:
        engineer_output_dublin.append(word[0])
        engineer_annual_dublin.append(word[1])
        engineer_daily_dublin.append(word[2])
    
    # calculate average for Dublin
    annual_average_engineer_dublin = (sum(engineer_annual_dublin)/len(engineer_annual_dublin))
    daily_average_engineer_dublin = (sum(engineer_daily_dublin)/len(engineer_daily_dublin))
    
    
for word in regional_area:
    if engineer in word[0]:
        engineer_output_regional.append(word[0])
        engineer_annual_regional.append(word[1])
        engineer_daily_regional.append(word[2])
    
    # Calculate average for non-Dublin area
    annual_average_engineer_regional = (sum(engineer_annual_regional)/len(engineer_annual_regional))
    daily_average_engineer_regional = (sum(engineer_daily_regional)/len(engineer_daily_regional))
    
# calculate difference between Dublin and region in percentage
daily_difference = (((daily_average_engineer_regional - daily_average_engineer_dublin)/daily_average_engineer_dublin)*100)
annual_difference = (((annual_average_engineer_regional - annual_average_engineer_dublin)/annual_average_engineer_dublin)*100)

# raw data
print(str(round(daily_difference)))  
print(str(round(annual_difference)))
print(engineer_output_regional) 
print(engineer_annual_regional)
print(engineer_daily_regional)
print(engineer_output_dublin) 
print(engineer_annual_dublin)
print(engineer_daily_dublin)


# User readable data output
print("Percentual difference between non-Dublin area and Dublin area in daily rate is {} %".format(str(round(daily_difference))))
print("Percentual difference between non-Dublin area and Dublin area in anual salary is {} %".format(str(round(annual_difference))))

print("The average annual salary for {} in Dublin area is {}".format(engineer,str(round(annual_average_engineer_dublin))))
print("The average annual salary for {} outside of the Dublin area is {}".format(engineer,str(round(annual_average_engineer_regional))))

print("The average daily rate salary for {} in Dublin area is {}".format(engineer,str(round(daily_average_engineer_dublin))))
print("The average daily rate salary for {} outside of the Dublin area is {}".format(engineer,str(round(daily_average_engineer_regional))))



        