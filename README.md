Assignment for simple python collection (tuple) and iteration with output.
As a starting data I used CPL software salary for position, annual salary 
for Dublin area, daily rate, annual salary for non-Dublin area and daily rate.
I tried to implement some easy search option which retrieves a software position 
with its salary rate. I also calculate a difference between regions.
Initially I was thinking too complicated and I used 2 tuples for Dublin and 
non Dublin area (work.py). Later I integrated data to the single tuple with 
all the operations.
The output:



['Software Engineer', 'QA Engineer', 'QA Automation Engineer']

[100000, 47000, 55000]

[90000, 43000, 51000]

[575, 325, 400]

[525, 300, 390]

-7

-9

Percentual difference between non-Dublin area and Dublin area in 
daily rate is -7 %

Percentual difference between non-Dublin area and Dublin area in anual 
salary is -9 %

The average annual salary for Engineer in Dublin area is 67333

The average annual salary for Engineer outside of the Dublin area is 61333

The average daily rate salary for Engineer in Dublin area is 433

The average daily rate salary for Engineer outside of the Dublin area is 405