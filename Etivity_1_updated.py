# list of the software positions in dublin and non-dublin area gathered from CPL web site. 
# Each position is stored in tuple with annual salary and daily rate for Dublin and region
# I demonstrate manipulation with tuple, creating dict to store separatelly each job, salary and daily rate for that particular area
# I calculate an average salary and daily rate and I compare the difference between these two geographic places

# tuple - my collection
software_area = [("software engineer", 100000, 575,90000, 525), ("technical architect", 87500, 575, 85000, 535),
               ("software team lead", 80000, 513, 72000, 500), 
               ("ui developer", 65000, 415, 58000, 400), ("mid level .net", 56500, 400, 51500, 375),
               ("c++ developer", 63000, 435, 58000, 415), 
               ("mid level python", 56500, 440, 52500, 410), ("android developer",56500, 440,53500, 400),
               ("game designer", 50000, 400, 48000, 350), 
               ("qa lead", 65000, 400, 58000, 380), ("qa engineer", 47000, 325, 43000, 300), 
               ("qa automation engineer", 55000, 400, 51000, 390)]

found_data = []
userdata = {"data":[]}
search_result = False

total_annual_dublin = 0
total_daily_dublin = 0
total_annual_regional = 0
total_daily_regional = 0


#collection
engineer_annual_dublin = []
engineer_annual_regional = []
engineer_daily_dublin = []
engineer_daily_regional = []
search_result_for_engineers = []


# search for a world in the collection. Empty string will calculate total data, any non case sensitive world will be searched.
# If word not found message displayed
# I have lowered the searching word just in the case. This convert any style to full lower case word

engineer = ('lEad').lower()
try:

    for word in software_area:

        if engineer in word[0]:
            search_result_for_engineers.append(word[0])
            found_data.append(word)
except:
    print("{} not in the database".format(engineer))
    search_result = False 
    
# creating a dict for storing data. My first dict creation in my live
if len(found_data)>0:
    # Flag for displaying data. If nothing found data is not displayed 
    search_result = True
    
    for p in found_data:
        dublin = {}
        region = {}   

        dublin["dublin_annual"] = p[1]
        dublin["dublin_daily"] = p[2]
        dublin["dublin_job"] = p[0]    
        userdata["data"].append(dublin)

        region["region_annual"] = p[3]
        region["region_daily"] = p[4]
        region["region_job"] = p[0]    
        userdata["data"].append(region)
        
    # iterate trought dict to find data for dublin and region (daily,annual)
    # I had problems to use build in function sum to gather sum of the collection. I sorted this problem with manual calculation    
    for x in userdata["data"]:
        if "dublin_daily" in x:
            total_daily_dublin +=int(x["dublin_daily"])
            engineer_daily_dublin.append(x["dublin_daily"])
        if "dublin_annual" in x:
            total_annual_dublin += int(x["dublin_annual"])
            engineer_annual_dublin.append(x["dublin_annual"])
        if "region_annual" in x:
            total_annual_regional += int(x["region_annual"])
            engineer_annual_regional.append(x["region_annual"])
        if "region_daily" in x:
            total_daily_regional += int(x["region_daily"])
            engineer_daily_regional.append(x["region_daily"])    
    

if search_result:
    
    # Data outputs
    print("The system found following software jobs on the base of your criteria:")    
    
    for i in search_result_for_engineers:
        print("{}".format(i))
    

    # Calculate average for Dublin
    annual_average_engineer_dublin = (total_annual_dublin/len(engineer_annual_dublin))
    daily_average_engineer_dublin = (total_daily_dublin/len(engineer_daily_dublin))

    # Calculate average for non-Dublin area
    annual_average_engineer_regional = (total_annual_regional/len(engineer_annual_regional))
    daily_average_engineer_regional = (total_daily_regional/len(engineer_daily_regional)) 

    
    
    # calculate difference between Dublin and region in percentage
    daily_difference = (((daily_average_engineer_regional - daily_average_engineer_dublin)/daily_average_engineer_dublin)*100)
    annual_difference = (((annual_average_engineer_regional - annual_average_engineer_dublin)/annual_average_engineer_dublin)*100)

            
    # User readable data output    
    print("Percentual difference between non-Dublin area and Dublin area in daily rate is {} %".format(str(round(daily_difference))))
    print("Percentual difference between non-Dublin area and Dublin area in anual salary is {} %".format(str(round(annual_difference))))

    print("The average annual salary for {} in Dublin area is {}".format(engineer,str(round(annual_average_engineer_dublin))))
    print("The average annual salary for {} outside of the Dublin area is {}".format(engineer,str(round(annual_average_engineer_regional))))

    print("The average daily rate salary for {} in Dublin area is {}".format(engineer,str(round(daily_average_engineer_dublin))))
    print("The average daily rate salary for {} outside of the Dublin area is {}".format(engineer,str(round(daily_average_engineer_regional))))

else:
    print("I am sorry, {} not in the database".format(engineer))

 



