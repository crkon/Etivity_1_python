# Problem statement:User can search trough positions to find the annual
# and/or daily rate for Dublin and non-Dublin positions. Code then calculates
# the average salary per region and also displays the difference between
# average salary in percentage.
# list of the software positions in dublin and non-dublin area gathered
# from CPL web site.
# Each position with annual salary and daily rate
# Implemented try except block - when position is not listen. Result
# displayed only when record found. When a searchin word is empty - all data
# is calculated. By searching for a single characted - code checks if
# character is presented in job title


software_area = [["software engineer", 100000, 575,90000, 525], ["technical architect", 87500, 575, 85000, 535],
               ["software team lead", 80000, 513, 72000, 500],
               ["ui developer", 65000, 415, 58000, 400], ["mid level .net", 56500, 400, 51500, 375],
               ["c++ developer", 63000, 435, 58000, 415],
               ["mid level python", 56500, 440, 52500, 410], ["android developer",56500, 440,53500, 400],
               ["game designer", 50000, 400, 48000, 350],
               ["qa lead", 65000, 400, 58000, 380], ["qa engineer", 47000, 325, 43000, 300],
               ["qa automation engineer", 55000, 400, 51000, 390]]

#collection
engineer_annual_dublin = []
engineer_annual_regional = []
engineer_daily_dublin = []
engineer_daily_regional = []
search_result_for_engineers = []



# search for world software. Empty string will calculate total data
# I have capitalized the searching word just in the case
# engineer = ('e')
engineer = ('LeAd').lower()
try:

    for word in software_area:

        if engineer in word[0]:
            search_result_for_engineers.append(word[0])
            engineer_annual_dublin.append(word[1])
            engineer_daily_dublin.append(word[2])
            engineer_annual_regional.append(word[3])
            engineer_daily_regional.append(word[4])

        # calculate average for Dublin
    annual_average_engineer_dublin = (sum(engineer_annual_dublin)/len(engineer_annual_dublin))
    daily_average_engineer_dublin = (sum(engineer_daily_dublin)/len(engineer_daily_dublin))

    # Calculate average for non-Dublin area
    annual_average_engineer_regional = (sum(engineer_annual_regional)/len(engineer_annual_regional))
    daily_average_engineer_regional = (sum(engineer_daily_regional)/len(engineer_daily_regional))
    #     if len(search_result_for_engineers) > 1:
    search_result = True


except:
    print("{} not in the tuples".format(engineer))
    search_result = False


if search_result:

    # calculate difference between Dublin and region in percentage
    daily_difference = (((daily_average_engineer_regional - daily_average_engineer_dublin)/daily_average_engineer_dublin)*100)
    annual_difference = (((annual_average_engineer_regional - annual_average_engineer_dublin)/annual_average_engineer_dublin)*100)

    # raw data
    print(search_result_for_engineers)

    print(engineer_annual_dublin)
    print(engineer_annual_regional)

    print(engineer_daily_dublin)
    print(engineer_daily_regional)

    print(str(round(daily_difference)))
    print(str(round(annual_difference)))


    # User readable data output
    print("Percentual difference between non-Dublin area and Dublin area in daily rate is {} %".format(str(round(daily_difference))))
    print("Percentual difference between non-Dublin area and Dublin area in anual salary is {} %".format(str(round(annual_difference))))

    print("The average annual salary for {} in Dublin area is {}".format(engineer,str(round(annual_average_engineer_dublin))))
    print("The average annual salary for {} outside of the Dublin area is {}".format(engineer,str(round(annual_average_engineer_regional))))

    print("The average daily rate salary for {} in Dublin area is {}".format(engineer,str(round(daily_average_engineer_dublin))))
    print("The average daily rate salary for {} outside of the Dublin area is {}".format(engineer,str(round(daily_average_engineer_regional))))


